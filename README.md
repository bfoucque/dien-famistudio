# README #

This repository is dedicated to the integration of the famistudio library in the neslib nesdoug C ecosystem

### Objectives ###

* Natively integrate the famistudio engine in C project
* Provide project template
* Provide how to

### Project Description ###

* There is two directories: Dienpong and Dienscroll (MMC1)
	
### Dienpong ###

Dienpong is a fully functional Pong like game with Famistudio support: music, SFX and DPCM

* UP or DOWN to move their sorcerers
* START will start a round or pause - unpause the game
* LEFT - RIGHT pushed during rebound throw the ball up or down
* SELECT switch music
* B pushed accelerate the player during 3 seconds, consuming a star
* A pushed during rebound accelerate the ball but consumme a star
* a star is won every 3 rebound

So it's fairly simple ;-)
 
The code has several options (wall rebound etc...).
The game is running on both emulators and hardware.

### Dienscroll ###

It is Dienpong but with MMC1 support. The code support music and SFX, the START during the game switch the mirroring mode of the screen.

To Do:
* Fix the Sample

### Current issue ###

* The DPCM support is not working

### Who do I talk to? ###

* Any personn who would like to help :-)