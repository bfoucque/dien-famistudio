#reference: https://nesdoug.com/2018/09/05/how-cc65-works/

#in the root
myMainFile="dienpong"

echo $myMainFile

#we clean
mv BUILD/$myMainFile.nes BUILD/PREVIOUS/$myMainFile.nes

#we compile
cc65 -Oirs $myMainFile.c --add-source

#Assember
ca65 crt0.s
ca65 $myMainFile.s -g

#we link
ld65 -C nrom_32k_vert.cfg -o $myMainFile.nes crt0.o $myMainFile.o nes.lib -Ln labels.txt

#we remove the .o 
rm -f *.o

#we move the result
mv $myMainFile.nes BUILD/$myMainFile.nes
mv $myMainFile.s BUILD/$myMainFile.s
mv labels.txt BUILD/labels.txt

open -a OpenEmu BUILD/$myMainFile.nes
