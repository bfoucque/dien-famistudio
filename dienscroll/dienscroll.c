/*	example code for cc65, for NES
 *  
 */	
 
//INCLUDES LIBS
#include "LIB/neslib.h"
#include "LIB/nesdoug.h"
//MUSIC
#include "LIB/famistudio.h"
//MMC1 Mapper
#include "MMC1/bank_helpers.h"
#include "MMC1/bank_helpers.c"


// ?????
#pragma bss-name(push, "ZEROPAGE")

//LEVELS
#include "LEVEL/level_test_1.h"

//SPRITES
#include "SPRITES/Sprites.h" // holds our metasprite data

//DEFINE AND INITIALIZE PALETTE

const unsigned char palette_bg[]={
0x0f,0x27,0x10,0x30, // grays
0x0f,0x01,0x21,0x31, // blues
0x0f,0x06,0x26,0x36, // reds
0x0f,0x09,0x29,0x39 }; // greens

const unsigned char palette_sp[]={
0x0f,0x27,0x10,0x30, // grays
0x0f,0x01,0x21,0x31, // blues
0x0f,0x06,0x26,0x36, // reds
0x0f,0x09,0x29,0x39 }; // greens 

const unsigned char title_color_rotate[]={
	0x32,0x22,0x30,0x37
};

// TEXTS
const unsigned char text[]="Sprites";
const unsigned char TITLE_TEXT[]="Dien Pong: Ultimate Pong";

// PADS
unsigned char pad1;
unsigned char pad1_new;

unsigned char pad2;
unsigned char pad2_new;

// GLOBAL VARIABLES
int address;

//GAME MODE
unsigned char game_state;
enum {MODE_TITLE, MODE_GAME, MODE_PAUSE, MODE_SWITCH, MODE_END, MODE_GAME_OVER, TRANSITION,TRANSITION_TITLE};


//max mana and touch
static unsigned char max_mana = 5;
static unsigned char mana_touch = 1;

//raq1
unsigned char raq1_position_x=8;
unsigned char raq1_position_y=120;
unsigned char raq1_speed=3;
unsigned char raq1_mana=1;
unsigned char raq1_touch=0;
unsigned char raq1_speedboost=0;
unsigned char raq1_speedboost_counter =0;
unsigned char raq1_score_unit=0;
unsigned char raq1_score_tens=0;

//raq2
unsigned char raq2_position_x=232;
unsigned char raq2_position_y=120;
unsigned char raq2_speed=3;
unsigned char raq2_mana=1;
unsigned char raq2_touch=0;
unsigned char raq2_speedboost=0;
unsigned char raq2_speedboost_counter =0;
unsigned char raq2_score_unit=0;
unsigned char raq2_score_tens=0;

//ball
unsigned char ball_position_x=128;
unsigned char ball_position_y=120;

unsigned char ball_direction_x=0x00;
unsigned char ball_direction_y=0x10;
unsigned char ball_speed=1;

//collision management
unsigned char collision;
struct BoxColl {
	unsigned char x;
	unsigned char y;
	unsigned char width;
	unsigned char height;
};
struct BoxColl BoxRaq1 = {8,120,8,8};
struct BoxColl BoxRaq2 = {232,120,8,8};
struct BoxColl BoxBall = {128,120,8,8};

//counter
unsigned char counter_1 =0;
unsigned char counter_2 =0;

//counter frame
unsigned char counter_frame =0;
unsigned char counter_frame_raq1 =0;
unsigned char counter_frame_raq2 =0;
//WALL
unsigned char wall_rebound=0x00;
unsigned char wall_raq=0x00;

//music
unsigned char song;

//miroring
unsigned char mirror = 0;

//SCROLL
unsigned int scroll_x = 0;
unsigned int scroll_y = 0;
// -----------------------------------------------------------------------------------------
// --------------------------        FUNCTIONS         -------------------------------------
// -----------------------------------------------------------------------------------------
void test_collision_raq1(void){
	collision = check_collision(&BoxRaq1, &BoxBall);
		
	// change the BG color, if sprites are touching
	if (collision){
		famistudio_sfx_sample_play(7);
		//pal_col(0,0x36);
		if ((raq1_mana<max_mana)&&(raq1_touch>=mana_touch))
		{
			++raq1_mana;
			raq1_touch=0;
			//famistudio_sfx_play(1,0);
			asm("	lda #1 \n 	ldx #0 \n	jsr _famistudio_sfx_play \n	"	);
		}
		else
		{
			++raq1_touch;
		}
		

		ball_direction_x =0x00;
		ball_position_x = 16;
		BoxBall.x = 16;
		if(pad1 & PAD_LEFT)
		{
			ball_direction_y =0x00;
		}
		else if (pad1 & PAD_RIGHT)
		{
			ball_direction_y =0x20;
		}
		else
		{
			ball_direction_y =0x10;
		}

		//we increase  the speed of the ball
		if((pad1 & PAD_A)&&(raq1_mana>0))
		{
			++ball_speed;
			--raq1_mana;
			raq1_touch=0;
			asm("	lda #0 \n 	ldx #0 \n	jsr _famistudio_sfx_play \n	"	);//play first sample
		}
		
	}
	else{
		//pal_col(0,0x00);
	}
}


void test_collision_raq2(void){
	collision = check_collision(&BoxRaq2, &BoxBall);
		
	// change the BG color, if sprites are touching
	if (collision){
		famistudio_sfx_sample_play(7);
		if ((raq2_mana<max_mana)&&(raq2_touch>=mana_touch))
		{
			++raq2_mana;
			raq2_touch=0;
			//famistudio_sfx_play(1,0);
			asm("	lda #1 \n 	ldx #0 \n	jsr _famistudio_sfx_play \n	"	);
		}
		else
		{
			++raq2_touch;
		}

		ball_direction_x =0x10;
		ball_position_x = 224;
		BoxBall.x = 224;
		if(pad2 & PAD_LEFT)
		{
			ball_direction_y =0x00;
		}
		else if (pad2 & PAD_RIGHT)
		{
			ball_direction_y =0x20;
		}
		else
		{
			ball_direction_y =0x10;
		}

		if((pad2 & PAD_A)&&(raq2_mana>0))
		{
			++ball_speed;
			--raq2_mana;
			raq2_touch=0;
			asm("	lda #0 \n 	ldx #0 \n	jsr _famistudio_sfx_play \n	"	);//play first sample
			//pal_col(0,0x00);
		}
	}
	else{
		//pal_col(0,0x00);
	}
}

void draw_hub()
{
			//THE HUD
			//We draw the number of  star
			//one_vram_buffer(127, NTADR_A(10,3));
			//we draw rqa2 the start score 
			counter_1=1;
			for (counter_1;counter_1<=raq1_mana;++counter_1)
			{one_vram_buffer(128, NTADR_A(2+counter_1,2));}
			//we clean when we decrease the start score 
			counter_1=raq1_mana+1;
			for (counter_1;counter_1<=max_mana;++counter_1)
			{one_vram_buffer(0, NTADR_A(2+counter_1,2));}
			//we draw rqa2 the start score 
			counter_1=1;
			for (counter_1;counter_1<=raq2_mana;++counter_1)
			{one_vram_buffer(128, NTADR_A(30-counter_1,2));}
			//we clean when we decrease the start score 
			counter_1=raq2_mana+1;
			for (counter_1;counter_1<=max_mana;++counter_1)
			{one_vram_buffer(0, NTADR_A(30-counter_1,2));}

			// the score
			//raq1
			one_vram_buffer(48+raq1_score_tens, NTADR_A(12,2));
			one_vram_buffer(48+raq1_score_unit, NTADR_A(13,2));
			// separator
			one_vram_buffer(45, NTADR_A(15,2));
			//raq2
			one_vram_buffer(48+raq2_score_tens, NTADR_A(17,2));
			one_vram_buffer(48+raq2_score_unit, NTADR_A(18,2));
}

// -----------------------------------------------------------------------------------------
// --------------------------MAIN: LET'S START THE MADNESS        --------------------------
// -----------------------------------------------------------------------------------------

void main (void) {
	
	ppu_off(); // screen off

	// load the palettes
	pal_bg(palette_bg);
	pal_spr(palette_sp);
	
	// use the second set of tiles for sprites
	// both bg and sprite are set to 0 by default
	bank_spr(1);

	// this sets a start position on the BG, top left of screen
	// vram_adr() and vram_unrle() need to be done with the screen OFF
	vram_adr(NAMETABLE_A);	
	
	// we set the vram buffer
	set_vram_buffer();
	//vram_unrle(test_nam);
	//vram_put(0);
	//vram_put('A');
	//vram_put('B');
	//vram_put('C');
	//vram_write(level_test_1,256);
	//vram_write(level_test_1,256);

	
	//NOW we manage the color
	//get_at_addr(char nt, char x, char y); // pixel positions 0-0xff
	/*
	address = get_at_addr(0,0,0);
	vram_adr(address); // nametable A's attribute table 23c0-23ff
	
	vram_fill(0, 8); // 8 bytes of 00 00 00 00
	vram_fill(0x55, 8); // 8 bytes of 01 01 01 01
	vram_fill(0xAA, 8); // 8 bytes of 10 10 10 10
	vram_fill(0xFF, 8); // 8 bytes of 11 11 11 11
	
	address = get_at_addr(0,0x40,0xa0);
	vram_adr(address); // pick a random attribute byte on the lower half
	
	vram_put(0xe4); // push 1 byte 11 10 01 00
	
					// note they go on the screen like this
					// 00  01
					// 10  11
	*/
	ppu_on_all(); // turn on screen
	//we strat inPAUSE mdeo
	game_state = MODE_TITLE;
	multi_vram_buffer_horz(TITLE_TEXT,sizeof(TITLE_TEXT), NTADR_A(4,13));//48,25,NTADR_A(4,2));

	song = 1;
	famistudio_music_play(song);
	//famistudio_music_pause(TRUE);

	ball_direction_y = 0x00;
	
	
	while (1){
		// infinite loop
		
		ppu_wait_nmi(); // wait till beginning of the frame
		// the sprites are pushed from a buffer to the OAM during nmi
		
		//Coubter frame
		++counter_frame;
		if (counter_frame==60)
			{
				counter_frame = 0;
			}
		//we read the pad
		pad1 = pad_poll(0); // read the first controller
		pad1_new = get_pad_new(0);

		pad2 = pad_poll(1); // read the first controller
		pad2_new = get_pad_new(1);

		// do at the beginning of each frame
		clear_vram_buffer();
		
		// clear all sprites from sprite buffer
		oam_clear();

		
		
		if ((game_state == MODE_GAME) || (game_state == MODE_PAUSE))
		{
			
			
			
			
			draw_hub();


			// push a single sprite
			// oam_spr(unsigned char x,unsigned char y,unsigned char chrnum,unsigned char attr);
			// use tile #0, palette #0

			if (counter_frame==0)
			{
				counter_frame_raq1=0;
				counter_frame_raq2=1;
			}
			else if (counter_frame==15)
			{
				counter_frame_raq1=1;
				counter_frame_raq2=2;
			}
			else if (counter_frame==30)
			{
				counter_frame_raq1=2;
				counter_frame_raq2=1;
			}
			else if (counter_frame==45)
			{
				counter_frame_raq1=1;
				counter_frame_raq2=0;
			}
			oam_spr(raq1_position_x, raq1_position_y, 48+counter_frame_raq1, 2);
			
			// push a metasprite
			// oam_meta_spr(unsigned char x,unsigned char y,const unsigned char *data);
			oam_spr(raq2_position_x, raq2_position_y, 48+counter_frame_raq2, 67);
			//oam_meta_spr(raq2_position_x, raq2_position_y, metasprite);
			
			// and another
			//oam_meta_spr(ball_position_x, ball_position_y, metasprite2);
			oam_spr(ball_position_x, ball_position_y, 4+counter_frame_raq1,1);


			//we manage the pause
			if((pad1_new & PAD_START)||(pad2_new & PAD_START))
			{
					if (game_state == MODE_PAUSE)
					{
						game_state = MODE_GAME;
						famistudio_music_pause(FALSE);
					}
					else
					{
						famistudio_music_pause(TRUE);
						game_state = MODE_PAUSE;
					}

					if((pad1 & PAD_A)||(pad2 & PAD_A))
						{
							game_state = TRANSITION_TITLE;
						}
					// miroring management
					if (mirror ==0)
						{
							set_mirroring(MIRROR_LOWER_BANK);
							++mirror;
						}
					else if (mirror ==1)
						{
							set_mirroring(MIRROR_UPPER_BANK);
							++mirror;
						}
					else if (mirror ==2)
						{
							set_mirroring(MIRROR_HORIZONTAL);
							++mirror;
						}
					else if (mirror ==3)
						{
							set_mirroring(MIRROR_VERTICAL);
							mirror=0;
						}
					
			}

			// we manage the select
			if((pad1_new & PAD_SELECT)||(pad2_new & PAD_SELECT))
			{
				//famistudio_sfx_play(1,0);
				//asm("	lda #0 \n 	ldx #0 \n	jsr _famistudio_sfx_play \n	"	);
				
				//sample
				//famistudio_sfx_sample_play(5);
				
				if (song ==0)
				{
					song = 1;
					famistudio_music_play(song);
				}
				else if (song ==1)
				{
					song = 2;
					famistudio_music_stop();
				}
				else
				{
					song = 0;
					famistudio_music_play(song);
				}
			}
			
			//We restart the game

			//we manage the bbost
			// boost player 1
			if (raq1_speedboost_counter == 0)
				{
					raq1_speed=3;
					if((pad1_new & PAD_B)&&(raq1_mana>0))
					{
						--raq1_mana;
						raq1_speedboost_counter=180;
					}
				}
			else 
			{
				--raq1_speedboost_counter;
				raq1_speed=9;
			}

			// boost player 2
			if (raq2_speedboost_counter == 0)
				{
					raq2_speed=3;
					if((pad2_new & PAD_B)&&(raq2_mana>0))
					{
						--raq2_mana;
						raq2_speedboost_counter=180;
					}
				}
			else 
			{
				--raq2_speedboost_counter;
				raq2_speed=9;
			}

			

			// now we manage the movement
			if(pad1 & PAD_UP)
			{
				// ok but no multiplication
				raq1_position_y = raq1_position_y - raq1_speed;
				BoxRaq1.y = BoxRaq1.y - raq1_speed;
			}
			else if (pad1 & PAD_DOWN)
			{
				// ok but no multiplication
				raq1_position_y = raq1_position_y + raq1_speed;
				BoxRaq1.y = BoxRaq1.y + raq1_speed;
			}
			
			if(pad2 & PAD_UP)
			{
				// ok but no multiplication
				raq2_position_y = raq2_position_y - raq2_speed;
				BoxRaq2.y = BoxRaq2.y - raq2_speed;
			}
			else if (pad2 & PAD_DOWN)
			{
				// ok but no multiplication	
				raq2_position_y = raq2_position_y + raq2_speed;
				BoxRaq2.y = BoxRaq2.y + raq2_speed;
			}

			//we block the raq if needed
			if (wall_raq ==0x00)
			{
				if (raq1_position_y < 24)
				{
					raq1_position_y=24;
					BoxRaq1.y = 24;
				}
				if (raq2_position_y < 24)
				{
					raq2_position_y=24;
					BoxRaq2.y = 24;
				}
				if (raq1_position_y > 224)
				{
					raq1_position_y=224;
					BoxRaq1.y = 224;
				}
				if (raq2_position_y > 224)
				{
					raq2_position_y=224;
					BoxRaq2.y = 224;
				}
			}

			//the rebound check
			if (wall_rebound == 0x00)
			{
				//we test the position of the ball
				if (ball_position_y < 24)
				{
					ball_position_y =24;
					BoxBall.y  = 24;
					ball_direction_y = 0x00;
				}
				if (ball_position_y > 224)
				{
					ball_position_y =224;
					BoxBall.y  = 224;
					ball_direction_y = 0x20;
				}
			}

			//The Ball Movement
			if (game_state == MODE_GAME)
			{
				if (ball_direction_x == 0x00)
				{
					ball_position_x =ball_position_x +  ball_speed;
					BoxBall.x = BoxBall.x+ ball_speed;
				}
				else
				{
					ball_position_x =ball_position_x -  ball_speed;
					BoxBall.x = BoxBall.x - ball_speed;
				}
				
				//ball orientation
				if (ball_direction_y == 0x00)
				{
					ball_position_y =ball_position_y +  ball_speed;
					BoxBall.y = BoxBall.y+ ball_speed;
				}
				else if (ball_direction_y == 0x20)
				{
					ball_position_y =ball_position_y -  ball_speed;
					BoxBall.y = BoxBall.y - ball_speed;
				}

				//we check the collision
				test_collision_raq1();
				test_collision_raq2();

				//test score
				//we test the position of the ball
				if (ball_position_x < 8)
				{
					//we reset the ball position
					ball_position_x =128;
					ball_position_y =120;
					BoxBall.x  = 128;
					BoxBall.y  = 120;
					ball_direction_y = 0x10;
					ball_direction_x = 0x10;
					//we reset ball_speed
					ball_speed = 1;
					//we freeze
					game_state = MODE_PAUSE;
					// increase score player 2
					++raq2_score_unit;
					if (raq2_score_unit >=10)
					{
						raq2_score_unit =0;
						++raq2_score_tens;
						if (raq2_score_tens >=10)
						{
							raq2_score_tens=0;
						}
					}
				}
				if (ball_position_x > 248)
				{
					//we reset the ball position
					ball_position_x =128;
					ball_position_y =120;
					BoxBall.x  = 128;
					BoxBall.y  = 120;
					ball_direction_y = 0x10;
					ball_direction_x = 0x00;
					//we reset ball_speed
					ball_speed = 1;
					//we freeze
					game_state = MODE_PAUSE;
					// increase score player 2
					++raq1_score_unit;
					if (raq1_score_unit >=10)
					{
						raq1_score_unit =0;
						++raq1_score_tens;
						if (raq1_score_tens >=10)
						{
							raq1_score_tens=0;
						}
					}
				}
			}
		}
	// ----------------------------------------------------------------------------------------------------
	// -------------------------- TITLE SCREEN ------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	else if (game_state == MODE_TITLE) 
		{
			counter_1 = get_frame_count();
			counter_1 = (counter_1 >> 3) & 3;
			pal_col(3,title_color_rotate[counter_1]);

			
			//for (counter_1 = 0;counter_1<=2;++counter_1)
			//	{one_vram_buffer(128, NTADR_A(0+counter_1,0));}
			
			
			
			//Background scroll
			//scroll_x += 1;
			
			// scroll y is tricky, since values 240-255 are treated as -16 to -1
			// add_scroll_y,sub_scroll_y will adjust 16 if cross this range
			scroll_y = sub_scroll_y(1, scroll_y);
			//OR scroll_y = add_scroll_y(1, scroll_y);

			// set scroll
			//set_scroll_x(scroll_x);
			set_scroll_y(scroll_y);

			if (ball_direction_x == 0x00)
				{
					ball_position_x =ball_position_x +  ball_speed;
					BoxBall.x = BoxBall.x+ ball_speed;
				}
			else
				{
					ball_position_x =ball_position_x -  ball_speed;
					BoxBall.x = BoxBall.x - ball_speed;
				}
				
				//ball orientation
			if (ball_direction_y == 0x00)
				{
					ball_position_y =ball_position_y +  ball_speed;
					BoxBall.y = BoxBall.y+ ball_speed;
				}
			else if (ball_direction_y == 0x20)
				{
					ball_position_y =ball_position_y -  ball_speed;
					BoxBall.y = BoxBall.y - ball_speed;
				}

			if (ball_position_y < 24)
				{
					ball_position_y =24;
					BoxBall.y  = 24;
					ball_direction_y = 0x00;
				}
			if (ball_position_y > 224)
				{
					ball_position_y =224;
					BoxBall.y  = 224;
					ball_direction_y = 0x20;
				}
			if (ball_position_x < 8)
				{
					ball_direction_x = 0x00;
				}
			if (ball_position_x > 248)
				{
					ball_direction_x = 0x10;
				}

			if (counter_frame==0)
			{
				counter_frame_raq1=0;
			}
			else if (counter_frame==15)
			{
				counter_frame_raq1=1;
			}
			else if (counter_frame==30)
			{
				counter_frame_raq1=2;
			}
			else if (counter_frame==45)
			{
				counter_frame_raq1=1;
			}
			
			oam_spr(ball_position_x, ball_position_y, 4+counter_frame_raq1,1);
			
			
			if((pad1_new & PAD_START)||(pad2_new & PAD_START))
			{
				game_state = TRANSITION;
			}
			
				
		}
		// ----------------------------------------------------------------------------------------------------
		// -------------------------- TRANSITION SCREEN ------------------------------------------------------------
		// ----------------------------------------------------------------------------------------------------
		else if (game_state == TRANSITION) 
		{
			set_scroll_y(0);
			pal_fade_to(4,0); // fade to black
		
			// I clean the VRAM
			//address = get_at_addr(0,0,0);
			ppu_off();
			for (counter_1=0;counter_1<32;++counter_1)//max 32
			{
				for (counter_2=0;counter_2<30;++counter_2) //max 30
					{
						clear_vram_buffer();
						one_vram_buffer(0, NTADR_A(counter_1,counter_2));
						flush_vram_update_nmi();
					}	
			}
			ppu_on_all();
			
			draw_hub();
			song = 0;
			famistudio_music_play(song);
			pal_bright(4);
			ball_direction_y = 0x10;
			ball_position_x =128;
			ball_position_y =120;
			BoxBall.x  = 128;
			BoxBall.y  = 120;
			ball_direction_y = 0x10;
			ball_direction_x = 0x00;
			raq1_mana = 1;
			raq2_mana = 1;

			game_state = MODE_PAUSE;
		}

		else if (game_state == TRANSITION_TITLE) 
		{
			set_scroll_y(0);
			pal_fade_to(4,0); // fade to black
		
			// I clean the VRAM
			//address = get_at_addr(0,0,0);
			ppu_off();
			for (counter_1=0;counter_1<32;++counter_1)//max 32
			{
				for (counter_2=0;counter_2<30;++counter_2) //max 30
					{
						clear_vram_buffer();
						one_vram_buffer(0, NTADR_A(counter_1,counter_2));
						flush_vram_update_nmi();
					}	
			}
			ppu_on_all();
			multi_vram_buffer_horz(TITLE_TEXT,sizeof(TITLE_TEXT), NTADR_A(4,13));//48,25,NTADR_A(4,2));
			ball_direction_y = 0x00;
			song = 1;
			famistudio_music_play(song);
			pal_bright(4);
			game_state = MODE_TITLE;
		}
	//END WHILE
	}
}


	
	