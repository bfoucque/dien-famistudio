//play a music in FamiTone format

void __fastcall__ famistudio_music_play(unsigned char song);

//stop music

void __fastcall__ famistudio_music_stop(void);

//pause and unpause music

void __fastcall__ famistudio_music_pause(unsigned char pause);

//play FamiTone sound effect on channel 0..3

void __fastcall__ famistudio_sfx_play(unsigned char sound,unsigned char channel);

//play a DPCM sample, 1..63

void __fastcall__ famistudio_sfx_sample_play(unsigned char sample);